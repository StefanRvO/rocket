#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <esp_log.h>
#include "drivers/DS18B20_Reader.h"
#include <vector>
#define DS18B20_PIN 5
#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)

std::vector<DS18B20_Reader> ds18b20_readers;
extern "C" void temperature_monitor_init(void);
extern "C" void temperature_monitor_task(void *pvParameters);

void temperature_monitor_init(void)
{
    ds18b20_readers.push_back(DS18B20_Reader(5, RMT_CHANNEL_1, RMT_CHANNEL_0));
}

void temperature_monitor_task(void *pvParameters)
{
    delay_ms(2000);
    for(auto reader : ds18b20_readers)
    {
        reader.start_sample();
        //reader.wait_for_samples();
    }
}
