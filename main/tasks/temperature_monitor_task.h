 
#pragma once
#include "freertos/task.h"

void temperature_monitor_init(void);
void temperature_monitor_task(void *pvParameters);
