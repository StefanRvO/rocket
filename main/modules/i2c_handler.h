#pragma once
#include <i2cdev.h>

void i2c_handler_init(void);
void i2c_handler_set_primary_address(uint8_t addr);
i2c_dev_t* i2c_handler_get_device(void);