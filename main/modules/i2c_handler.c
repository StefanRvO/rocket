#include <i2cdev.h>
#include "i2c_handler.h"

#define SDA_GPIO (gpio_num_t)14
#define SCL_GPIO (gpio_num_t)27
#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)
#define I2C_FREQ_HZ 400000 // 400KHz
static i2c_dev_t dev;


void i2c_handler_init(void)
{
    i2cdev_init();
    dev.port = (i2c_port_t)0;
    dev.cfg.sda_io_num = SDA_GPIO;
    dev.cfg.scl_io_num = SCL_GPIO;
    dev.cfg.master.clk_speed = I2C_FREQ_HZ;

    i2c_dev_create_mutex(&dev);
}

void i2c_handler_set_primary_address(uint8_t addr)
{
    dev.addr = addr;
}

i2c_dev_t *i2c_handler_get_device(void)
{
    return &dev;
}