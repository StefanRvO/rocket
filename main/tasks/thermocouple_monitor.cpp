#include "drivers/SC18IS602.h"

extern "C" void thermocouple_monitor_task(void *pvparamters);
extern "C" void thermocouple_monitor_init(void);
#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)

static SC18IS602 *spi_device = nullptr;

void thermocouple_monitor_task(void *pvparameters)
{
    while(true)
    {
        uint8_t data_out[10] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10};
        uint8_t data_in[10];
        spi_device->spi_transfer(0x02, data_out, data_in, 10);
        delay_ms(100);
        printf("write\n");
    }
}

void thermocouple_monitor_init(void)
{
    spi_device = new SC18IS602(0x00);
}