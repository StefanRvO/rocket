#include <list>
#include "owb.h"
#include "owb_rmt.h"
#include "ds18b20.h"

class DS18B20_Reader
{
    public:
        DS18B20_Reader(uint8_t pin_num, rmt_channel_t tx_channel, rmt_channel_t rx_channel);
        void start_sample();
        void wait_for_samples();
        uint8_t get_device_cnt();

    private:
        OneWireBus * owb = nullptr;
        owb_rmt_driver_info rmt_driver_info;
        std::list<OneWireBus_ROMCode> device_rom_codes;
        std::list<DS18B20_Info *> devices;
        uint8_t pin_num;
        float *samples;
        uint32_t *error_cnt;
};