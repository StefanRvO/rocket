#include "SC18IS602.h"

extern "C" {
    #include <i2cdev.h>
    #include "modules/i2c_handler.h"
}
#include <cstring>
#define CPOL_BIT (1 << 3)
#define CPHA_BIT (1 << 2)

enum SC18IS602_spi_freq_t
{
    SPI_FREQ_1843KHZ = 0,
    SPI_FREQ_461KHZ  = 1,
    SPI_FREQ_115KHZ  = 2,
    SPI_FREQ_58KHZ   = 3,
};

enum SC18IS602_functions_t
{
    FUNC_SPI_WRITE = 0x00,
    FUNC_SPI_CONFIG = 0xF0,
    FUNC_CLEAR_INT = 0xF1,
    FUNC_IDLE   = 0xF2,
    FUNC_GPIO_WRITE = 0xF4,
    FUNC_GPIO_READ  = 0xF5,
    FUNC_GPIO_ENABLE = 0xF6,
    FUNC_GPIO_CONFIG = 0xF7,
};

SC18IS602::SC18IS602(uint8_t slave_addr_tritet)
{
    this->dev = i2c_handler_get_device();
    this->slave_addr = (0x05 << 3) | (slave_addr_tritet);
    this->setup_device();
}


void SC18IS602::setup_device()
{
    uint8_t data_out[10];
    data_out[0] = (uint8_t)FUNC_SPI_CONFIG;
    data_out[1] = (uint8_t)SPI_FREQ_1843KHZ;
    i2c_dev_take_mutex(this->dev);
    i2c_dev_write_addr(this->dev, NULL, 0, data_out, 2, this->slave_addr);
    i2c_dev_give_mutex(this->dev);
}

void SC18IS602::spi_transfer(uint8_t ss_mask, uint8_t *data_out, uint8_t *data_in, uint8_t size)
{
    uint8_t out_buff[256] = {0};
    memcpy(out_buff + 1, data_in, size);
    out_buff[0] = FUNC_SPI_WRITE | ss_mask;
    i2c_dev_take_mutex(this->dev);
    i2c_dev_read_addr(this->dev, out_buff, (uint16_t)size + 1, data_in, size, this->slave_addr);
    i2c_dev_give_mutex(dev);
}

    