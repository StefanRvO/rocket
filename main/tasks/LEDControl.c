#include "LEDControl.h"
#include "drivers/ws2812.h"
#include <string.h>
#include <driver/rmt.h>

#define LED_COUNT 20
#define LED_PIN_1 13
#define LED_PIN_2 12
#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)

static rgbVal leds_1[LED_COUNT] = {};
static rgbVal leds_2[LED_COUNT] = {};

void led_control_init(void)
{
    ws2812_init(LED_PIN_1, RMT_CHANNEL_0);
    ws2812_init(LED_PIN_2, RMT_CHANNEL_1);

    memset(leds_1, 0x00, sizeof(leds_1));  
    memset(leds_2, 0x00, sizeof(leds_2));  

}

void led_control_task(void *pvParameters)
{
    uint64_t cnt = 0;
    while(1)
    {
        memset(leds_1, 0x00, sizeof(leds_1));  
        memset(leds_2, 0x00, sizeof(leds_2));  
        for(uint8_t i = 0; i < LED_COUNT; i++)
        {
            leds_1[i].r = 255 * (cnt == 0);
            leds_1[i].g = 255 * (cnt == 1);
            leds_1[i].b = 255 * (cnt == 2);
            leds_2[i].g = 255 * (cnt == 0);
            leds_2[i].r = 255 * (cnt == 1);
            leds_2[i].b = 255 * (cnt == 2);
        }
        cnt = (cnt + 1) % 4;
        ws2812_setColors(LED_COUNT, leds_1, RMT_CHANNEL_0);
        ws2812_setColors(LED_COUNT, leds_2, RMT_CHANNEL_1);
        delay_ms(200);
    }
}