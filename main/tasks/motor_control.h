#pragma once
void motor_control_init(void);
void motor_control_task(void *pvParameters);
void motor_control_set_speed(float speed);