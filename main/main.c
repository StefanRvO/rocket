#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "freertos/event_groups.h"
#include <soc/rmt_struct.h>
#include <esp_system.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>
#include <nvs_flash.h>
#include <driver/gpio.h>
#include <stdio.h>
#include <lwip/err.h>
#include <lwip/sys.h>
#include <apps/sntp/sntp.h>
#include <time.h>

#include "tasks/motor_control.h"
#include "tasks/temperature_monitor_task.h"
#include "tasks/LEDControl.h"
#include "tasks/external_pwm_control.h"
#include "modules/sd_card_handler.h"
#include "modules/i2c_handler.h"
#include "tasks/thermocouple_monitor.h"

static EventGroupHandle_t wifi_event_group;
#define ESP_WIFI_SSID      "PLACEHOLDER"
#define ESP_WIFI_PASS      "PLACEHOLDER"

const int WIFI_CONNECTED_BIT = BIT0;
static const char *TAG = "Rocket Control main";
#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)


void wifi_init_sta();
static esp_err_t event_handler(void *ctx, system_event_t *event);
void wifi_init_sta();

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "got ip:%s",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        xEventGroupSetBits(wifi_event_group, WIFI_CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_AP_STACONNECTED:
        ESP_LOGI(TAG, "station:"MACSTR" join, AID=%d",
                 MAC2STR(event->event_info.sta_connected.mac),
                 event->event_info.sta_connected.aid);
        break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
        ESP_LOGI(TAG, "station:"MACSTR"leave, AID=%d",
                 MAC2STR(event->event_info.sta_disconnected.mac),
                 event->event_info.sta_disconnected.aid);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, WIFI_CONNECTED_BIT);
        break;
    default:
        break;
    }
    return ESP_OK;
}

void wifi_init_sta()
{
    wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = ESP_WIFI_SSID,
            .password = ESP_WIFI_PASS
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");
    ESP_LOGI(TAG, "connect to ap SSID:%s password:%s",
             ESP_WIFI_SSID, ESP_WIFI_PASS);
}

static void initialize_sntp(void)
{
    ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_init();
}

void thermocouple_monitor_task(void *pv_parameters);
void thermocouple_monitor_init(void);


void app_main()
{
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);
  ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");

  wifi_init_sta();
  setenv("TZ", "CET-1CES", 1);
  tzset();
  initialize_sntp();
  //motor_control_init();
  //temperature_monitor_init();
  i2c_handler_init();
  //extern_pwm_control_init();
  thermocouple_monitor_init();
  //sd_card_module_init();
  led_control_init();
  //xTaskCreate(motor_control_task, "PWM Task", 4096, NULL, 10, NULL);
  xTaskCreate(led_control_task, "LED Task", 4096, NULL, 10, NULL);
  //xTaskCreate(extern_pwm_control_task, "EPWM Task", 4096, NULL, 10, NULL);
  xTaskCreate(thermocouple_monitor_task, "THERM Task", 4096, NULL, 10, NULL);  
  //xTaskCreate(temperature_monitor_task, "Temp Task", 4096, NULL, 10, NULL);

  delay_ms(3000);
  //motor_control_set_speed(1);


  return;
}
