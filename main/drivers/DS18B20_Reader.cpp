#include "DS18B20_Reader.h"
#include <esp_log.h>

#define DS18B20_RESOLUTION (DS18B20_RESOLUTION_12_BIT)
static char TAG[] = "DS18B20_Reader";

DS18B20_Reader::DS18B20_Reader(uint8_t _pin_num, rmt_channel_t tx_channel, rmt_channel_t rx_channel)
{
    this->pin_num = _pin_num;
    this->owb = owb_rmt_initialize(&this->rmt_driver_info, this->pin_num, tx_channel, rx_channel);
    OneWireBus_SearchState search_state = {0};
    bool found = false;
    owb_search_first(this->owb, &search_state, &found);
    while (found)
    {
        char rom_code_s[17];
        owb_string_from_rom_code(search_state.rom_code, rom_code_s, sizeof(rom_code_s));
        this->device_rom_codes.push_back(search_state.rom_code);
        ESP_LOGI(TAG, "Found DS18B20  %d : %s", this->device_rom_codes.size(), rom_code_s);
        owb_search_next(this->owb, &search_state, &found);
    }
    ESP_LOGI(TAG, "Found %d device%s\n", device_rom_codes.size(), device_rom_codes.size() == 1 ? "" : "s");
    for (OneWireBus_ROMCode rom_code : this->device_rom_codes)
    {
        DS18B20_Info * ds18b20_info = ds18b20_malloc();
        devices.push_back(ds18b20_info);
        ds18b20_init(ds18b20_info, this->owb, rom_code); // associate with bus and device
        ds18b20_use_crc(ds18b20_info, true); // enable CRC check for temperature readings
        ds18b20_set_resolution(ds18b20_info, DS18B20_RESOLUTION);
    }
    this->samples = new float[this->device_rom_codes.size()];
    this->error_cnt = new uint32_t[this->device_rom_codes.size()];
}

void DS18B20_Reader::start_sample()
{
    ds18b20_convert_all(this->owb);
}

void DS18B20_Reader::wait_for_samples()
{
    DS18B20_ERROR *errors = new DS18B20_ERROR[this->devices.size()];
    ds18b20_wait_for_conversion(this->devices.front());
    uint8_t i = 0;
    for(DS18B20_Info *ds18b20_device : this->devices)
    {
        errors[i] = ds18b20_read_temp(ds18b20_device, &this->samples[i]);
        i++;
    }
    for(uint8_t i = 0; i < this->devices.size(); i++)
    {
        if(errors[i] != DS18B20_OK)
        {
            ++this->error_cnt[i];
        }
        ESP_LOGI(TAG, "  %d: %.1f    %d errors\n", i, this->samples[i], this->error_cnt[i]);
    }
    delete[] errors;
}

uint8_t DS18B20_Reader::get_device_cnt()
{
    return this->device_rom_codes.size();
}

