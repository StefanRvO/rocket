#include "external_pwm_control.h"
#include "pca9685.h"
#include "modules/i2c_handler.h"
#define ADDR PCA9685_ADDR_BASE
#define SDA_GPIO (gpio_num_t)15
#define SCL_GPIO (gpio_num_t)4
#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)

#define CHECK_LOOP(X, msg, ...) do { esp_err_t __; while((__ = X) != ESP_OK) { printf(msg "\n", ## __VA_ARGS__); vTaskDelay(250 / portTICK_PERIOD_MS); }} while (0)
static i2c_dev_t *dev;

void extern_pwm_control_init(void)
{    
    dev = i2c_handler_get_device();
    i2c_handler_set_primary_address(0x50);
    CHECK_LOOP(pca9685_init(dev),
            "Could not init PCA9685");
    CHECK_LOOP(pca9685_restart(dev),
            "Could not restart");

    CHECK_LOOP(pca9685_set_pwm_frequency(dev, 1000),
            "Could not set PWM frequency");
    uint16_t freq;
    CHECK_LOOP(pca9685_get_pwm_frequency(dev, &freq),
            "Could not get PWM frequency");
    printf("Freq 1000Hz, real %d\n", freq);

}

void extern_pwm_control_task(void *pvParameters)
{
    uint16_t val = 300;
    while(1)
    {
        printf("Set ch0 to %d, ch4 to %d\n", val, 4096 - val);
        CHECK_LOOP(pca9685_set_pwm_value(dev, 0, val),
                "Could not set PWM value to ch0");
        CHECK_LOOP(pca9685_set_pwm_value(dev, 4, 4096 - val),
                "Could not set PWM value to ch4");
        delay_ms(50);
    }
}