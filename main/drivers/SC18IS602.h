#pragma once
//Driver for interfacing with the I2C to SPI bridge IC SC18IS602
//Currently reserves an entire port for this chip. May be changed later.
#include <i2cdev.h>

class SC18IS602
{   
    public:
        SC18IS602(uint8_t slave_addr_tritet);
        void register_interupt_func();
        void spi_transfer(uint8_t ss_mask, uint8_t *data_out, uint8_t *data_in, uint8_t size);
    private:
        i2c_dev_t *dev;
        uint8_t slave_addr;
        void setup_device();
};

