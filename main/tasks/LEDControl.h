#pragma once
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

void led_control_init(void);
void led_control_task(void *pvParameters);
